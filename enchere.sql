-- create database encheredb;
-- create role enchere login password 'enchere';
-- alter database encheredb owner to enchere;

-- table
CREATE TABLE Enchere (
    id SERIAL NOT NULL primary key, 
    description varchar(255), 
    duree time(7), 
    dateDebut date, 
    prixMin int4, 
    photoEnchereid int4 NOT NULL, 
    Statueid int4 NOT NULL, 
    Produitid int4 NOT NULL
);
CREATE TABLE photoEnchere (
    id SERIAL NOT NULL,
    photo varchar(255), 
    PRIMARY KEY (id)
);
CREATE TABLE EnchereClient (
    id SERIAL NOT NULL, 
    montant int4, 
    "date" date, 
    idClient int4 NOT NULL, 
    idEnchere int4 NOT NULL, 
    PRIMARY KEY (id)
);
CREATE TABLE Commission (
    id SERIAL NOT NULL, 
    ValeurCommission int4, 
    EnchereClientid int4 NOT NULL, 
    PRIMARY KEY (id)
);
CREATE TABLE Client (
    id SERIAL NOT NULL, 
    Nom varchar(255), 
    Prenom varchar(255), 
    Email varchar(255), 
    Mdp varchar(255), 
    Contact int4, 
    Solde int4, 
    PRIMARY KEY (id)
);
CREATE TABLE Admin (
    id SERIAL NOT NULL, 
    Email varchar(255), 
    Mdp varchar(255), 
    PRIMARY KEY (id)
);
CREATE TABLE Categorie (
    id SERIAL NOT NULL, 
    Nom varchar(255), 
    PRIMARY KEY (id)
);
CREATE TABLE Produit (
    id SERIAL NOT NULL, 
    Nom varchar(255), 
    Categorieid int4 NOT NULL, 
    PRIMARY KEY (id)
);
CREATE TABLE Statue (
    id SERIAL NOT NULL, 
    Statue varchar(255), 
    PRIMARY KEY (id)
);
ALTER TABLE Enchere ADD CONSTRAINT FKEnchere194561 FOREIGN KEY (photoEnchereid) REFERENCES photoEnchere (id);
ALTER TABLE EnchereClient ADD CONSTRAINT FKEnchereCli753162 FOREIGN KEY (idClient) REFERENCES Client (id);
ALTER TABLE Commission ADD CONSTRAINT FKCommission363073 FOREIGN KEY (EnchereClientid) REFERENCES EnchereClient (id);
ALTER TABLE Produit ADD CONSTRAINT FKProduit762078 FOREIGN KEY (Categorieid) REFERENCES Categorie (id);
ALTER TABLE Enchere ADD CONSTRAINT FKEnchere209032 FOREIGN KEY (Statueid) REFERENCES Statue (id);
ALTER TABLE Enchere ADD CONSTRAINT FKEnchere390744 FOREIGN KEY (Produitid) REFERENCES Produit (id);
ALTER TABLE EnchereClient ADD CONSTRAINT FKEnchereCli201200 FOREIGN KEY (idEnchere) REFERENCES Enchere (id);

CREATE TABLE RechargementCompte(
    id SERIAL NOT NULL, 
    DateRechargementCompte date,
    idClient int NOT NULL, 
    Montant int NOT NULL, 
    Etat int NOT NULL
);
ALTER TABLE RechargementCompte ADD CONSTRAINT FKRechargement FOREIGN KEY(idclient) REFERENCES Client (id);


/*client*/
insert into Client(Nom,Prenom,Email,Mdp,Contact,solde) values('Rakoto','Rabe','Rakoto@gmail.com','rakoto123',0341512345,1000000);
insert into Client(Nom,Prenom,Email,Mdp,Contact,solde) values('Jean','Arthur','Jean@gmail.com','jean456',0331546585,3000000);
insert into Client(Nom,Prenom,Email,Mdp,Contact,solde) values('Rasoa','elisabeth','Rasoa@gmail.com','rasoa789',0324528621,100000);
insert into Client(Nom,Prenom,Email,Mdp,Contact,solde) values('Rasoa','Jeanne','RasoaJ@gmail.com','rasoaJ',0324528611,100000);
insert into Client(Nom,Prenom,Email,Mdp,Contact,solde) values('Rakoto','Rado','Rado@gmail.com','rado',0324528221,100000);
insert into Client(Nom,Prenom,Email,Mdp,Contact,solde) values('Rakoto','Ando','Ando@gmail.com','ando',0324528121,100000);
insert into Client(Nom,Prenom,Email,Mdp,Contact,solde) values('Rabe','Elisa','Elisa@gmail.com','elisa',0334528621,100000);
insert into Client(Nom,Prenom,Email,Mdp,Contact,solde) values('Rabe','Soa','Soa@gmail.com','soa',0334528621,100000);

/*photoEnchere*/
insert into photoEnchere(id,photo) values(1,'1231dd');
insert into photoEnchere(id,photo) values(2,'12hvg');
insert into photoEnchere(id,photo) values(3,'1321lkj');
insert into photoEnchere(id,photo) values(4,'1hjk');
insert into photoEnchere(id,photo) values(5,'fgyfv');

/*statue*/
insert into Statue(id,Statue) values(1,'A venir');
insert into Statue(id,Statue) values(2,'En cours');
insert into Statue(id,Statue) values(3,'Terminé');

/*Catégories*/
insert into Categorie(Nom) values('Livres anciens et de collection');
insert into Categorie(Nom) values('Art et antiquités');
insert into Categorie(Nom) values('Matériel de bricolage');
insert into Categorie(Nom) values('Jeux vidéo et consoles ');
insert into Categorie(Nom) values('Monnaies');
insert into Categorie(Nom) values('Vêtements et accessoires des dessinées,commics et produits dérivés');
insert into Categorie(Nom) values('Articles de collection');
insert into Categorie(Nom) values('Jouets et jeux');
insert into Categorie(Nom) values('Bijoux et montres');
insert into Categorie(Nom) values('Gastronomie et boissons');
insert into Categorie(Nom) values('Timbres');

/*Produit*/
insert into Produit(Nom,Categorieid) values('Voiture',1);
insert into Produit(Nom,Categorieid) values('livres',1);
insert into Produit(Nom,Categorieid) values('téléphones',1);
insert into Produit(Nom,Categorieid) values('Galerie',2);
insert into Produit(Nom,Categorieid) values('Art',2);
insert into Produit(Nom,Categorieid) values('Matériel',3);
insert into Produit(Nom,Categorieid) values('jeux vidéo',4);
insert into Produit(Nom,Categorieid) values('Console',4);
insert into Produit(Nom,Categorieid) values('Coins',5);
insert into Produit(Nom,Categorieid) values('Vêtements',6);
insert into Produit(Nom,Categorieid) values('Plate',7);
insert into Produit(Nom,Categorieid) values('Jouets',8);
insert into Produit(Nom,Categorieid) values('Jeux',8);
insert into Produit(Nom,Categorieid) values('Bijoux',9);
insert into Produit(Nom,Categorieid) values('montres',9);
insert into Produit(Nom,Categorieid) values('Coffee',10);
insert into Produit(Nom,Categorieid) values('Timbres',11);

-- truncate enchere restart identity;
--enchere
insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Livre spécialisé véhicules de service ferroviaire allemands, boussole type avec de nombreuses images, NEUF','10:00:00','11-01-2023',100000,1,3,2);
insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Verre européen catalogue de vente aux enchères 69, catalogues verre','12:00:00','2023-01-16',200000,2,2,5);
insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Femme bijoux mode bijoux collier collier frange perle collier bijoux rares','09:00:00','2023-02-25',100000,3,1,14);
insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Montre automatique Rolex Submariner Date 16610 SEL cadran noir avec B&P $ Pas de prix de réserve aux enchères !','09:30:00','2023-03-12',100000,4,1,15);
insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('2016 BMW 3-Series','02:15:00','2023-02-05',11900,5,1,1);

-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Ancien jeux video','03:15:00','07-02-2023',1100,5,1,7);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Tableau','04:15:00','08-02-2023',1900,5,1,5);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Robe','01:15:00','09-02-2023',1190,5,1,10);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Soutient','02:25:00','10-02-2023',1900,5,1,10);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Renault 4','02:35:00','11-02-2023',119000,5,1,1);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Cadillac','05:45:00','12-02-2023',11900000,5,1,1);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Asus Rog','06:15:00','13-02-2023',11900,5,1,6);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Huawei p20 pro','07:15:00','14-02-2023',1190000,5,1,3);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('The begining after the end','08:15:00','15-02-2023',1010900,5,1,2);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Mozzart','09:15:00','16-02-2023',11900,5,1,4);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Bethoven','20:15:00','17-02-2023',25000,5,1,5);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Salon en cuir','21:15:00','18-02-2023',119000,5,1,6);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Elden Ring 4','22:15:00','19-02-2023',9000,5,1,7);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('PS4','23:15:00','20-02-2023',11000,5,1,8);
-- insert into Enchere(description,duree,dateDebut,prixMin,photoEnchereid,Statueid,Produitid) values('Bon Coins','20:15:00','21-02-2023',10900,5,1,9);

/*enchere client MANOMBOKA ETO NDRAY !!! */
insert into EnchereClient(montant,date,idClient,idEnchere) values(1200000,'2023-02-25',2,3);
insert into EnchereClient(montant,date,idClient,idEnchere) values(1300000,'2023-02-25',1,3);
insert into EnchereClient(montant,date,idClient,idEnchere) values(1400000,'2023-02-25',3,3);
insert into EnchereClient(montant,date,idClient,idEnchere) values(1500000,'2023-02-25',2,3);
insert into EnchereClient(montant,date,idClient,idEnchere) values(1600000,'2023-02-25',2,3);
insert into EnchereClient(montant,date,idClient,idEnchere) values(1650000,'2023-02-25',3,3);
insert into EnchereClient(montant,date,idClient,idEnchere) values(1700000,'2023-02-25',4,3);

insert into EnchereClient(montant,date,idClient,idEnchere) values(500000,'2023-01-16',3,2);
insert into EnchereClient(montant,date,idClient,idEnchere) values(600000,'2023-01-16',1,2);
insert into EnchereClient(montant,date,idClient,idEnchere) values(700000,'2023-01-16',2,2);
insert into EnchereClient(montant,date,idClient,idEnchere) values(800000,'2023-01-16',2,2);
insert into EnchereClient(montant,date,idClient,idEnchere) values(900000,'2023-01-16',2,2);
insert into EnchereClient(montant,date,idClient,idEnchere) values(910000,'2023-01-16',3,2);
insert into EnchereClient(montant,date,idClient,idEnchere) values(920000,'2023-01-16',4,2);
insert into EnchereClient(montant,date,idClient,idEnchere) values(930000,'2023-01-16',5,2);
insert into EnchereClient(montant,date,idClient,idEnchere) values(950000,'2023-01-16',6,2);

insert into EnchereClient(montant,date,idClient,idEnchere) values(300000,'2023-01-11',1,1);
insert into EnchereClient(montant,date,idClient,idEnchere) values(400000,'2023-01-11',2,1);
insert into EnchereClient(montant,date,idClient,idEnchere) values(500000,'2023-01-11',3,1);
insert into EnchereClient(montant,date,idClient,idEnchere) values(600000,'2023-01-11',1,1);
insert into EnchereClient(montant,date,idClient,idEnchere) values(650000,'2023-01-11',4,1);

/*commission*/
insert into Commission(ValeurCommission,EnchereClientid) values(10000,1);
insert into Commission(ValeurCommission,EnchereClientid) values(2000,2);
insert into Commission(ValeurCommission,EnchereClientid) values(30000,3);

/*Admin*/
insert into Admin(Email,Mdp) values('Echere@gmail.com','mdp123');

/*RechargementCompte*/
insert into RechargementCompte(DateRechargementCompte,idClient,Montant,Etat) values('2023-02-25',1,100000,0);
insert into RechargementCompte(DateRechargementCompte,idClient,Montant,Etat) values('2023-01-30',2,20000,0);
insert into RechargementCompte(DateRechargementCompte,idClient,Montant,Etat) values('2023-02-16',3,230000,0);

-- -- VIEW 

-- v_rechargementCompte
create or replace view v_rechargementCompteClient as
select
RechargementCompte.id,
RechargementCompte.idClient,
client.nom,
client.prenom,
client.email,
client.solde,
RechargementCompte.DateRechargementCompte,
RechargementCompte.montant
from
RechargementCompte
join client
on RechargementCompte.idClient = client.id
where RechargementCompte.etat=0;

-- -- v_produitCategorie
create or replace view V_ProduitCategorie as 
select
produit.*,
categorie.nom as nomCategorie
from produit
join categorie
on produit.categorieid = categorie.id ;

-- stat nbre acheteur par produit
create or replace view V_NbreAcheteurProduit as
select
enchere.id as idEnchere,
enchere.description,
enchere.produitid,
V_ProduitCategorie.nom as nomProduit,
V_ProduitCategorie.nomCategorie,
count(distinct(idclient)) as nbAcheteur
from 
enchere
join EnchereClient
on enchere.id = EnchereClient.idEnchere
join V_ProduitCategorie
on enchere.produitid = V_ProduitCategorie.id
where enchere.Statueid>=2
group by 
enchere.id,
V_ProduitCategorie.nom,
V_ProduitCategorie.nomCategorie
order by enchere.id asc;

-- v_enchere
create or replace view V_Enchere as 
select
enchere.id,
enchere.description,
enchere.duree,
enchere.dateDebut,
enchere.prixMin,
enchere.idClient as idClientAuteur,
Statue.statue,
-- EnchereClient.idClient as idClientAcheteur,
Client.prenom as acheteur,
EnchereClient.montant,
EnchereClient.date as dateMise
from 
enchere
join Statue
on enchere.Statueid = Statue.id
join EnchereClient
on enchere.id = EnchereClient.idEnchere
join Client
on enchereClient.idClient = client.id
order by id asc;




