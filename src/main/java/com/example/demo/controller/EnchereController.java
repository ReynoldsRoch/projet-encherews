/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.controller;

import com.example.demo.dao.CategorieDAO;
import com.example.demo.dao.ProduitDAO;
import com.example.demo.model.Categorie;
import com.example.demo.model.Produit;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author ReynoldsComputer
 */
@Controller
public class EnchereController {

    @Autowired
    CategorieDAO categorieDAO;

    @Autowired
    ProduitDAO produitDAO;

    // insert
    @RequestMapping(value = "/produits", method = RequestMethod.POST, produces = "application/json")
    @CrossOrigin
    @ResponseBody
    public int insertProduit(@RequestParam(value = "nom", defaultValue = "") String nom,
            @RequestParam(value = "idCategorie", defaultValue = "1") int idCategorie) {
        Produit p = new Produit();
        p.setNom(nom);
        p.setIdCategorie(idCategorie);
        return produitDAO.insert(p);
    }

    @RequestMapping(value = "/categories", method = RequestMethod.POST, produces = "application/json")
    @CrossOrigin
    @ResponseBody
    public int insertCategorie(@RequestParam(value = "nom", defaultValue = "") String nom) {
        Categorie c = new Categorie();
        c.setNom(nom);
        return categorieDAO.insert(c);
    }

    // select
    @RequestMapping(value = "/categories", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public List<Categorie> listCategorie() {
        return categorieDAO.select();
    }

    @RequestMapping(value = "/produits", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @CrossOrigin
    public List<Produit> listProduit() {
        return produitDAO.select();
    }

}
