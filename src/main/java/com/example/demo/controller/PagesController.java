package com.example.demo.controller;

import com.example.demo.dao.AdminDAO;
import com.example.demo.dao.CategorieDAO;
import com.example.demo.dao.ProduitDAO;
import com.example.demo.model.V_RechargementCompteClient;
import java.util.HashMap;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

@Controller
public class PagesController {

    @Autowired
    AdminDAO adminDAO;

    @Autowired
    CategorieDAO categorieDAO;

    @Autowired
    ProduitDAO produitDAO;

    @RequestMapping(value = "/rechargerCompteClient", method = RequestMethod.GET, produces = "application/json")
    @CrossOrigin
    public String rechargerCompte(Model model, @RequestParam(value = "id") int id, @RequestParam(value = "idClient") int idClient, @RequestParam(value = "montant") int montant) {
        int validationRechargement = adminDAO.validerRechargement(id);

        if (validationRechargement == 1) {
            int rechargement = adminDAO.rechargerCompte(idClient, montant);
        }

        return this.rechargementCompte(model);
    }

    @RequestMapping(value = "/rechargement", method = RequestMethod.GET, produces = "application/json")
    @CrossOrigin
    public String rechargementCompte(Model model) {
        List<V_RechargementCompteClient> list = adminDAO.listRechargementCompte();
        model.addAttribute("rechargementComptes", list);
        return "pages/rechargement";
    }

    @RequestMapping(value = "/statistique", method = RequestMethod.GET, produces = "application/json")
    @CrossOrigin
    public String stat(Model model) {
        model.addAttribute("nbAcheteurProduits", adminDAO.listNbAcheteurProduit());
        return "pages/statistique";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = "application/json")
    @CrossOrigin
    public String checkLogin(@RequestParam(value = "email") String email, @RequestParam(value = "mdp") String mdp, Model model) {
        HashMap<String, Object> login = this.login(email, mdp);

        if (login.get("message").equals("Correct")) {
            return this.accueil(model);
        }

        return this.login();

    }

    public HashMap<String, Object> login(String email, String mdp) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        int checkLogin = 0;

        if (adminDAO.checkLogin(email, mdp).size() != 0) {
            checkLogin = adminDAO.checkLogin(email, mdp).get(0).getId();
        }

        if (checkLogin != 0) {
            result.put("message", "Correct");

//            MyToken myToken = new MyToken();
//            String tokenSociete = myToken.generateToken(checkLogin);
//            result.put("token", tokenSociete);
//            Date dateExpiDate = myToken.expirationdateToken(tokenSociete);
//
//            System.out.println();
//            System.out.println();
//            System.out.println("today's date : " + new Date(System.currentTimeMillis()));
//            System.out.println("expi date : " + dateExpiDate);
//            System.out.println();
//            System.out.println();
//
//            result.put("dateExpiration", dateExpiDate);
        } else {
            result.put("message", "Not Correct");
        }

        return result;
    }

    @GetMapping("/accueil")
    @CrossOrigin
    public String accueil(Model model) {
        model.addAttribute("categories", categorieDAO.select());
        return "pages/accueil";
    }

    @GetMapping("/")
    @CrossOrigin
    public String login() {
        return "pages/login";
    }

    @RequestMapping(value = "/greeting/{name}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String greeting(@PathVariable String name) {
        return "hello " + name;
    }
}
