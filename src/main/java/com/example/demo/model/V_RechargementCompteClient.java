/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.model;

import java.sql.Date;

/**
 *
 * @author ReynoldsComputer
 */
public class V_RechargementCompteClient {
    private int id;
    private int idClient;
    private String nom;
    private String prenom;
    private String email;
    private int solde;
    private Date dateRechargementCompte;
    private int montant;

    public int getSolde() {
        return solde;
    }

    public void setSolde(int solde) {
        this.solde = solde;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateRechargementCompte() {
        return dateRechargementCompte;
    }

    public void setDateRechargementCompte(Date dateRechargementCompte) {
        this.dateRechargementCompte = dateRechargementCompte;
    }

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }

    public V_RechargementCompteClient() {
    }
    
}
