/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.model;

/**
 *
 * @author ReynoldsComputer
 */
public class V_NbreAcheteurProduit {
    private int idEnchere;
    private String description;
    private int produitId;
    private String nomProduit;
    private String nomCategorie;
    private int nbAcheteur;

    public V_NbreAcheteurProduit() {
    }

    public int getIdEnchere() {
        return idEnchere;
    }

    public void setIdEnchere(int idEnchere) {
        this.idEnchere = idEnchere;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProduitId() {
        return produitId;
    }

    public void setProduitId(int produitId) {
        this.produitId = produitId;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public String getNomCategorie() {
        return nomCategorie;
    }

    public void setNomCategorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
    }

    public int getNbAcheteur() {
        return nbAcheteur;
    }

    public void setNbAcheteur(int nbAcheteur) {
        this.nbAcheteur = nbAcheteur;
    }
    
}
