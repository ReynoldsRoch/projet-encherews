/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.model;

import java.sql.Date;

/**
 *
 * @author ReynoldsComputer
 */
public class RechargementCompte extends BaseModel {
    private Date dateRechargementCompte;
    private int idClient;
    private int montant;
    private int etat;

    public Date getDateRechargementCompte() {
        return dateRechargementCompte;
    }

    public void setDateRechargementCompte(Date dateRechargementCompte) {
        this.dateRechargementCompte = dateRechargementCompte;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }

    public int getEtat() {
        return etat;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public RechargementCompte() {
    }
}
