package com.example.demo.dao;

import com.example.demo.model.Admin;
import com.example.demo.model.V_NbreAcheteurProduit;
import com.example.demo.model.V_RechargementCompteClient;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * AdminDAO
 */
@Repository
public class AdminDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public int rechargerCompte(int idClient, int montant){
        return jdbcTemplate.update("update client set solde = (select solde from client where id = '"+idClient+"')+ "+montant+" where id = '"+idClient+"';");
    }

    public int validerRechargement(int idRechargement) {
        int result = 0;

        result = jdbcTemplate.update("update rechargementCompte set etat=1 where id = '"+idRechargement+"';");

        return result;
    }

    public List<V_RechargementCompteClient> listRechargementCompte() {
        return jdbcTemplate.query("select*from v_rechargementCompteClient;",
                BeanPropertyRowMapper.newInstance(V_RechargementCompteClient.class));
    }

    public List<V_NbreAcheteurProduit> listNbAcheteurProduit() {
        return jdbcTemplate.query("select*from v_nbreAcheteurProduit order by nbacheteur desc;",
                BeanPropertyRowMapper.newInstance(V_NbreAcheteurProduit.class));
    }

    public List<Admin> checkLogin(String email, String mdp) {
        // System.out.println("login admin");
        return jdbcTemplate.query("select * from admin where email = '" + email + "' and mdp = '" + mdp + "'",
                BeanPropertyRowMapper.newInstance(Admin.class));
    }

}
