/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.dao;

import com.example.demo.model.Categorie;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ReynoldsComputer
 */

@Repository
public class CategorieDAO {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    public List<Categorie> select(){
        return jdbcTemplate.query("select * from categorie",
                BeanPropertyRowMapper.newInstance(Categorie.class));
    }
    
    public int insert(Categorie c){
        return jdbcTemplate.update("insert into categorie(nom) values ('"+c.getNom()+"'); ");
    }
    
}
