/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.dao;

import com.example.demo.model.Produit;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ReynoldsComputer
 */
@Repository
public class ProduitDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Produit> select() {
        return jdbcTemplate.query("select * from produit",
                BeanPropertyRowMapper.newInstance(Produit.class));
    }

    public int insert(Produit p) {
        return jdbcTemplate.update("insert into produit(nom,categorieid) values ('" + p.getNom() + "','" + p.getIdCategorie() + "'); ");
    }

}
